# Device image for frieza

This repository contains the *prebuilt*, *binary* objects which are packed into the device tree for the BQ M10 FHD, codenamed `frieza`. Not to be confused with the BQ M10 HD, codenamed `cooler`.
